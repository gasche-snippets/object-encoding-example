type iset_methods = {
  is_empty: bool;
  contains: int -> bool;
  insert: int -> iset;
  union: iset -> iset;
}
and iset = unit -> iset_methods

let is_empty s = (s ()).is_empty
let contains s n = (s ()).contains n

let rec insert s n =
  if (s ()).contains n then s
  else
    let rec this () = {
      is_empty = false;
      contains = (fun i -> i = n || contains s i);
      insert = insert this;
      union = union this;
    } in this

and union s1 s2 =
  if is_empty s1 then s2
  else if is_empty s2 then s1
  else
    let rec this () = {
      is_empty = false;
      contains = (fun i -> contains s1 i || contains s2 i);
      insert = insert this;
      union = union this
    } in this
;;

let rec empty_set () = {
  is_empty = true;
  contains = (fun _ -> false);
  insert = (fun i -> insert empty_set i);
  union = (fun s -> s)
}

(* empty_set.insert(3).union(empty_set.insert(1)).insert(5).contains(...) *)
let x1 = contains (insert (union (insert empty_set 3) (insert empty_set 1)) 5) 1
let x2 = contains (insert (union (insert empty_set 3) (insert empty_set 1)) 5) 2
let x3 = contains (insert (union (insert empty_set 3) (insert empty_set 1)) 5) 3
let x4 = contains (insert (union (insert empty_set 3) (insert empty_set 1)) 5) 4
let x5 = contains (insert (union (insert empty_set 3) (insert empty_set 1)) 5) 5
